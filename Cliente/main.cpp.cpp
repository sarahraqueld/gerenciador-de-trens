﻿#include <cstdio>      
//printf
#include <cstring>      
//memset
#include <cstdlib>      
//exit

#include <netinet/in.h> 
//htons
#include <arpa/inet.h>  
//inet_addr

#include <sys/socket.h> 
//socket

#include <unistd.h>     
//close
//Includes da medição analógica

#include <fstream>

#include <string>

#include <sstream>




#define MAXMSG 1024

#define MAXNAME 100

#define PORTNUM 4325

// Função de leitura do potenciometro
int readAnalog(int number){
   
stringstream ss;
  
 ss << PATH_ADC << number << "_raw";
  
 fstream fs;
  
 fs.open(ss.str().c_str(), fstream::in);
   
fs >> number;
   fs.close();
   
// Verificar faixas de valores do potenciometro e taxas de velocidade
if(number < x){
return 200;
}
else if(number > x && number > y){
return 400;
} 
return 600;

}



//Medição analógica

#define PATH_ADC "/sys/bus/iio/devices/iio:device0/in_voltage"




class Mensagem {
    
	public:

        char msg[MAXMSG];

        char nome[MAXNAME];

        int idade;

        Mensagem();
};


Mensagem::Mensagem()
{

    idade = 20;

}


int main(int argc, char *argv[])
{


//adicionar loop while


    struct sockaddr_in endereco;

    int socketId;


    Mensagem mensagem;

	int acao;
while(){
		
    cin << acao;
	//Desconectar-se do servidor
	if( acao == 1){
	// Desconecta 
	cout >> "Desconectado do servidor." >> endl;
	}
	// Ligar/desligar todos os trens
	else if(acao == 2){
		// Desliga
	cout >> "Todos os trens foram desligados." >> endl;
	}
	// Ligar/desligar um trem específico
	else if(acao == 3){
	cout >> "Que trem deseja ligar/desligar?" >> endl;
	cint << acao;
	}
	else if(acao == 4){
		cout >> "Escolha a velocidade do trem no seu computador e então insira o número do trem que deseja alterar a velocidade" >> endl;
		cin << acao;
		//envia mensagem de mudança de velocidade
	}	
 
    int valor = readAnalog(1);
 
    strcpy(mensagem.msg, valor);

    strcpy(mensagem.nome,"MacGyver");

    mensagem.idade = 100;

    int bytesenviados;


    /*
     * Configurações do endereço
    */

    memset(&endereco, 0, sizeof(endereco));

    endereco.sin_family = AF_INET;

    endereco.sin_port = htons(PORTNUM);

    endereco.sin_addr.s_addr = inet_addr("127.0.0.1");


    /*
     * Criando o Socket
     *
     * PARAM1: AF_INET ou AF_INET6 (IPV4 ou IPV6)

     * PARAM2: SOCK_STREAM ou SOCK_DGRAM
     * PARAM3: protocolo (IP, UDP, TCP, etc).
 Valor 0 escolhe automaticamente
    */
    
socketId = socket(AF_INET, SOCK_STREAM, 0);


    //Verificar erros

    if (socketId == -1)
    {

        printf("Falha ao executar socket()\n");

        exit(EXIT_FAILURE);

    }

    
//Conectando o socket cliente ao socket servidor

    if ( connect (socketId, (struct sockaddr *)&endereco, sizeof(struct sockaddr)) == -1 )
    {

        printf("Falha ao executar connect()\n");

        exit(EXIT_FAILURE);

    }
    printf ("Cliente conectado ao servidor\n");


    //Enviar uma msg para o cliente que se conectou

    printf("Cliente vai enviar uma mensagem\n");

    bytesenviados = send(socketId,&mensagem,sizeof(mensagem),0);


    if (bytesenviados == -1)
    {

        printf("Falha ao executar send()");

        exit(EXIT_FAILURE);

    }

    printf("Cliente enviou a seguinte msg (%d bytes) para o servidor: %s \n",bytesenviados,mensagem.msg);


    close(socketId);


}
    return 0;

}
