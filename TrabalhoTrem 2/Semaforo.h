#ifndef SEMAFORO_H
#define SEMAFORO_H

#include <sys/types.h>    //semget()
#include <sys/ipc.h>      //semctl()
#include <sys/sem.h>      //semop()

class Semaforo{

public:
  //chave, valor inicial, flags
  Semaforo(key_t,int,int);
  ~Semaforo();
  void P();
  void V();
  int getSemaforoId();
  int getContador();

private:
  union semun{int val;} arg;
  int semaforoId;
  struct sembuf estrOperacao;

};

#endif // SEMAFORO_H