#ifndef TREM_H
#define TREM_H

#include <QObject>
#include <thread>
#include <chrono>
#include <iostream>
#include "Semaforo.h"

using namespace std;

class Trem : public QObject
{
    Q_OBJECT
public:
    Trem(int,int,int);
    ~Trem();
    void start();
    void run();
    void toogleEstado();
    void setVelocidade(int);
    void setEnable(bool);
    void suspender();

    Semaforo *sem1;
    Semaforo *sem2;
    Semaforo *sem3;

signals:
    void updateGUI(int,int,int);

private:

   std::thread threadTrem1;
   std::thread threadTrem2;
   std::thread threadTrem3;
   std::thread threadTrem4;

   int id;
   int x;
   int y;
   int velocidade;
   bool enable;
};

#endif // TREM_H



