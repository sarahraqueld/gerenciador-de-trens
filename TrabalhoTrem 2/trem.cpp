#include "trem.h"

Trem::Trem(int id, int x, int y)
{
    this->id = id;
    this->x = x;
    this->y = y;
    velocidade = 500;
    enable = true;
}

Trem::~Trem()
{
    threadTrem1.join();
    threadTrem2.join();
    threadTrem3.join();
    threadTrem4.join();
}

void Trem::setVelocidade(int velocidade)
{
    this->velocidade = velocidade;
}

void Trem::toogleEstado()
{
    if(this->enable == false)
        this->enable = true;
    else
        this->enable = false;
}


void Trem::setEnable(bool enable)
{
    this->enable = enable;
}

void Trem::start()
{
    threadTrem1 = std::thread(&Trem::run,this);
    threadTrem2 = std::thread(&Trem::run,this);
    threadTrem3 = std::thread(&Trem::run,this);
    threadTrem4 = std::thread(&Trem::run,this);
}

void Trem::suspender(){
    //TREM 1
        if(id == 1) {

             if(x == 150 and y == 100) {
                 sem1->P();
             }

             if (x == 290 and y == 100) {
                 sem1->V();
             }
        }

        // TREM 2
        else if(id == 2) {
            //
           if(x == 150 and y == 130) {
                 sem2->P();
                 sem1->P();
           }

             if(x == 290 and y == 140) {
                 sem1->V();
             }

             if(x == 290 and y == 220) {
                 sem2->V();
             }
        }
        // TREM 3
        if(id == 3) {
            // semaforo 2
           if(x == 290 and y == 220) {
               sem2->P();
               sem3->P();
           }
           if(x == 430 and y == 210){
               sem2->V();
           }

           if(x == 290 and y == 210) {
                sem3->V();
           }
        }
        else {
            if(x == 290 and y == 330) {
               sem3->P();
            }
            if(x == 430 and y == 330) {
               sem3->V();
            }
        }
}

void Trem::run()
{
    while(true){
        switch(id){
        case 1:
            if (enable)
            {
                emit updateGUI(id,x,y);
                if (x < 290 && y == 120){
                    x+=10;
                }
                else if (x == 150 && y < 120){
                    y+=10;
                }
                else if (x > 150 && y == 20){
                    x-=10;
                }
                else{
                    y-=10;

                }
            }
            break;

        case 2:
            if (enable)
            {
                emit updateGUI(id,x,y);
                if (y == 120 && x < 290) //120
                    x+=10;
                else if (x == 290 && y < 220)
                    y+=10;
                else if (x > 150 && y == 220)
                    x-=10;
                else
                    y-=10;
             }

                break;

        case 3:
               if (enable)
               {
                   emit updateGUI(id,x,y);
                   if (y == 120 && x < 430) //120
                       x+=10;
                   else if (x == 430 && y < 220)    //150 120
                       y+=10;
                   else if (x > 290 && y == 220)    //150 20
                       x-=10;
                   else
                       y-=10;
               }
               break;
        case 4:
               if (enable)
               {
                   emit updateGUI(id,x,y);
                   if (y == 220 && x < 430)
                       x+=10;
                   else if (x == 430 && y < 320)
                       y+=10;
                   else if (x > 290 && y == 320)
                       x-=10;
                   else
                       y-=10;
               }
               break;
           default:
               break;
           }

           suspender();
           this_thread::sleep_for(chrono::milliseconds(velocidade));
       }
   }
