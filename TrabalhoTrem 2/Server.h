#ifndef SERVER_H
#define SERVER_H
#include <cstdio>       //printf
#include <cstring>      //memset
#include <cstdlib>      //exit
#include <netinet/in.h> //htons
#include <arpa/inet.h>  //inet_addr
#include <sys/socket.h> //socket
#include <unistd.h>     //close
#include <thread>
#include <iostream>
#include <vector>

using namespace std;

#define MAXMSG 1024
#define MAXNAME 100
#define PORTNUM 4329

class Server
{
public:
    Server();
    int socketId;
};

class Mensagem {
    public:
        //char msg[MAXMSG];
        int velocidade;
        char nome[MAXNAME];
        int acao;
        int trem;
        Mensagem();
};

#endif // SERVER_H
