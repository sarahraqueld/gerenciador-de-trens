#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "trem.h"
#include "Semaforo.h"
#include "Server.h"
#include <QMainWindow>
#include <QCloseEvent>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <cstdio>       //printf
#include <cstring>      //memset
#include <cstdlib>      //exit
#include <netinet/in.h> //htons
#include <thread>
#include <iostream>
#include <vector>

//#define MAXMSG 1024
//#define MAXNAME 100
//#define PORTNUM 4327

using namespace std;

namespace Ui {
class MainWindow;
}


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void contadorSemaforo();
    void closeEvent();
    void serverHandler();
    void changeVelocidade(int id, int velocidade);
    static void serverHandler (MainWindow *window, int conexaoClienteID, Mensagem mensagem);
    void socketHandler(int socketDescriptor,Mensagem mensagem);
    int socketId;
public slots:
    void updateInterface(int,int,int);
private:
    Ui::MainWindow *ui;
    Trem *trem1;
    Trem *trem2;
    Trem *trem3;
    Trem *trem4;
    Server *server;
    Semaforo *sem1;
    Semaforo *sem2;
    Semaforo *sem3;

    std::thread t;
};

#endif // MAINWINDOW_H
