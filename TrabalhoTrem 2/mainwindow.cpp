#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

 //   server = new Server();
//-----------------------------------------


    //variáveis do servidor
    struct sockaddr_in endereco;


    //variáveis relacionadas com as conexões clientes
    struct sockaddr_in enderecoCliente;
    socklen_t tamanhoEnderecoCliente = sizeof(struct sockaddr);
    int conexaoClienteId;

    //mensagem enviada pelo cliente
    Mensagem mensagem;

    /*
     * Configurações do endereço
    */
    memset(&endereco, 0, sizeof(endereco));
    endereco.sin_family = AF_INET;
    endereco.sin_port = htons(PORTNUM);
    //endereco.sin_addr.s_addr = INADDR_ANY;
    endereco.sin_addr.s_addr = inet_addr("10.7.170.137");

    /*
     * Criando o Socket
     *
     * PARAM1: AF_INET ou AF_INET6 (IPV4 ou IPV6)
     * PARAM2: SOCK_STREAM ou SOCK_DGRAM
     * PARAM3: protocolo (IP, UDP, TCP, etc). Valor 0 escolhe automaticamente
    */
    socketId = socket(AF_INET, SOCK_STREAM, 0);

    //Verificar erros
    if (socketId == -1)
    {
        printf("Falha ao executar socket()\n");
        exit(EXIT_FAILURE);
    }

    //Conectando o socket a uma porta. Executado apenas no lado servidor
    if ( bind (socketId, (struct sockaddr *)&endereco, sizeof(struct sockaddr)) == -1 )
    {
        printf("Falha ao executar bind()\n");
        exit(EXIT_FAILURE);
    }

    //Habilitando o servidor a receber conexoes do cliente
    if ( listen( socketId, 10 ) == -1)
    {
        printf("Falha ao executar listen()\n");
        exit(EXIT_FAILURE);
    }

    //servidor ficar em um loop infinito
    while(1)
    {
        printf("Servidor: esperando conexões clientes\n");

        //Servidor fica bloqueado esperando uma conexão do cliente
        conexaoClienteId = accept( socketId,(struct sockaddr *) &enderecoCliente,&tamanhoEnderecoCliente);
        //disparar a thread
        std::thread t(socketHandler,conexaoClienteId,mensagem);
        t.detach();
    }


    // ---------------------------SOCKET ---------------------



//--------------------------------------------
    ui->setupUi(this);

    trem1 = new Trem(1,170,20);
    connect(trem1,SIGNAL(updateGUI(int,int,int)),SLOT(updateInterface(int,int,int)));
    trem1->start();

    trem2 = new Trem(2,250,220);    //250 220
    connect(trem2,SIGNAL(updateGUI(int,int,int)),SLOT(updateInterface(int,int,int)));
    trem2->start();

    trem3 = new Trem(3,400,120);    //430 160
    connect(trem3,SIGNAL(updateGUI(int,int,int)),SLOT(updateInterface(int,int,int)));
    trem3->start();

    trem4 = new Trem(4,380,320);
    connect(trem4,SIGNAL(updateGUI(int,int,int)),SLOT(updateInterface(int,int,int)));
    trem4->start();

    sem1 = new Semaforo(1,1,IPC_CREAT|0600);
    sem2 = new Semaforo(2,1,IPC_CREAT|0600);
    sem3 = new Semaforo(3,1,IPC_CREAT|0600);

    trem1->sem1 = sem1;
    trem1->sem2 = sem2;
    trem1->sem3 = sem3;

    trem2->sem1 = sem1;
    trem2->sem2 = sem2;
    trem2->sem3 = sem3;

    trem3->sem1 = sem1;
    trem3->sem2 = sem2;
    trem3->sem3 = sem3;

    trem4->sem1 = sem1;
    trem4->sem2 = sem2;
    trem4->sem3 = sem3;

    t = std::thread(&MainWindow::t, this);

}

MainWindow::~MainWindow()
{
    t.join();
    delete ui;
}

void MainWindow::closeEvent(){
    t.detach();
    delete sem1;
    delete sem2;
    delete sem3;
}

void MainWindow::changeVelocidade(int id, int velocidade){
   if(id == 1){
       trem1->setVelocidade(velocidade);
   }
  else if(id == 2){
       trem2->setVelocidade(velocidade);
   }
}

void MainWindow::contadorSemaforo(){
    ui->cont1->display(sem1->getContador());
    ui->cont2->display(sem2->getContador());
    ui->cont3->display(sem3->getContador());
}

void socketHandler(int socketDescriptor,Mensagem mensagem)
{
    int byteslidos;

    //Verificando erros
    if ( socketDescriptor == -1)
    {
        printf("Falha ao executar accept()");
        exit(EXIT_FAILURE);
    }

    //receber uma msg do cliente
    //printf("Servidor vai ficar esperando uma mensagem\n");
    byteslidos = recv(socketDescriptor,&mensagem,sizeof(mensagem),0);

    if (byteslidos == -1)
    {
        printf("Falha ao executar recv()");
        exit(EXIT_FAILURE);
    }
    else if (byteslidos == 0)
    {
        printf("Cliente finalizou a conexão\n");
        exit(EXIT_SUCCESS);
    }

    //printf("Servidor recebeu a seguinte msg do cliente [%s:%d]: %s \n",mensagem.nome,mensagem.idade,mensagem.msg);

    close(socketDescriptor);
}

void MainWindow::updateInterface(int id, int x, int y)
{
    switch(id){
        case 1:
            contadorSemaforo();
            ui->labelTrem1->setGeometry(x,y,20,20);
            break;
        case 2:
            contadorSemaforo();
            ui->labelTrem2->setGeometry(x,y,20,20);
            break;
       case 3:
            contadorSemaforo();
            ui->labelTrem3->setGeometry(x,y,20,20);
            break;
        case 4:
            contadorSemaforo();
            ui->labelTrem4->setGeometry(x,y,20,20);
            break;

        default:
            break;
    }
}


